package com.samip.myapplication.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.samip.myapplication.R;
import com.samip.myapplication.util.Const;

import java.util.ArrayList;
import java.util.HashMap;

public class UserListAdapter extends BaseAdapter {
    Context context;
    int position;
    ArrayList<HashMap<String, Object>> userList;

    public UserListAdapter(Context context, ArrayList<HashMap<String, Object>> userList) {
        this.context = context;
        this.userList = userList;
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        Log.d("UserActivityLog:::::", "" + userList);


        View view1 = LayoutInflater.from(context).inflate(R.layout.view_row_user_list, null);
        TextView tvName = view1.findViewById(R.id.tvLstName);
        TextView tvEmail = view1.findViewById(R.id.tvLstEmail);
        TextView tvGender = view1.findViewById(R.id.tvLstGender);

        tvName.setText(userList.get(position).get(Const.First_name) + " " + userList.get(position).get(Const.Last_name));
        tvEmail.setText(String.valueOf(userList.get(position).get(Const.Email_id)));
        tvGender.setText(String.valueOf(userList.get(position).get(Const.Gender)));

        Log.d("gender::::::", "" + tvGender);

        Log.d("ViewLog:::::", view1.toString());

        Log.d("PositionLog:::::", "" + position);

        return view1;


    }
}

